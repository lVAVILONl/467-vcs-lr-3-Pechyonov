import java.util.ArrayList;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

public class NavalMine extends Unit {
    //for AI
    // ���
    public static int explode(Cell cell, PlayerAI player) {
        if(cell == null)
            throw new FieldNotSetException("Cell with this mine either doesn't exist or not set!");

        AtomicInteger returnStatement = new AtomicInteger();

        ArrayList<Cell> cellsToExplode = getAllExplodedNeighbours(cell);

        cellsToExplode.forEach(c -> {
            if(checkExploded(c))
                returnStatement.getAndIncrement();
            player.addHittedCell(c);
        });

        return returnStatement.get();
    }

    //for Alive
    public static int explode(Cell cell) {
        if(cell == null)
            throw new FieldNotSetException("Cell with this mine either doesn't exist or not set!");

        AtomicInteger returnStatement = new AtomicInteger();

        ArrayList<Cell> cellsToExplode = getAllExplodedNeighbours(cell);

        cellsToExplode.forEach(c -> {
            if(checkExploded(c))
                returnStatement.getAndIncrement();
        });

        return returnStatement.get();
    }

    public static ArrayList<Cell> getAllExplodedNeighbours(Cell cell) {
        ArrayList<Cell> cellsToExplode = new ArrayList<>();

        for (Map.Entry<Direction, Cell> entry : cell.getNeighborCells().entrySet()) {
            //adding navalMine neighbours
            cellsToExplode.add(entry.getValue());
            //and their neighbours
            for (Map.Entry<Direction, Cell> innerEntry : entry.getValue().getNeighborCells().entrySet()) {
                if(innerEntry.getValue().getUnitType() != UnitType.NAVAL_MINE && !cellsToExplode.contains(innerEntry.getValue())) {
                    cellsToExplode.add(innerEntry.getValue());
                }
            }
        }

        return cellsToExplode;
    }

    private static boolean checkExploded(Cell c) {
        boolean isAShip = false;

        if(c.getUnitType() == UnitType.SHIP_PART){
            c.hit();
            c.getUnit().destroy();
            c.setUnit(UnitType.BROKEN_SHIP_PART);

            isAShip = true;
        }
        else if (c.getUnitType() == UnitType.ISLAND) {
            c.hit();
            c.getUnit().destroy();
        }
        else {
            c.hit();
        }

        return isAShip;
    }
}
