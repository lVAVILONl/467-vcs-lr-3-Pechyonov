public class Player {
    /**
     * Player's name
     */
    private String name = "NoName";

    /**
     * Field for active player check
     */
    private boolean isActive = false;

    /**
     * Player's game
     */
    protected Game game;

    public Player() { }

    public boolean makeMove(Cell cell) {
        boolean gotHit = false;

        cell.hit();
        if(cell.getUnitType() == UnitType.SHIP_PART)
        {
            cell.getUnit().destroy();
            cell.setUnit(UnitType.BROKEN_SHIP_PART);
            game.getAIGameField().decreaseUnbrokenShipParts();

            gotHit = true;
        }
        else if(cell.getUnitType() == UnitType.NAVAL_MINE) {
            cell.getUnit().destroy();
            int hittedShipParts = NavalMine.explode(cell);

            if(hittedShipParts > 0) {
                gotHit = true;
                for(int i=0; i<hittedShipParts; i++) {
                    game.getAIGameField().decreaseUnbrokenShipParts();
                }
            }
        }
        else if (cell.getUnitType() == UnitType.ISLAND) {
            cell.getUnit().destroy();
        }
        else if (cell.getUnitType() == UnitType.LIGHTHOUSE) {
            cell.getUnit().destroy();
        }

        return gotHit;
    }

    public void setName(String name) {
        if(name.isEmpty())
            throw new IllegalArgumentException("Player's name can't be empty!");
        this.name = name; }

    public String getName() { return name; }

    public void setActive(boolean isActive) { this.isActive = isActive; }

    public boolean getIfActive() { return isActive; }

    public void setGame(Game game) { this.game = game; }
}